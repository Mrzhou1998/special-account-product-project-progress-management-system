/*
 Navicat Premium Data Transfer

 Source Server         : C##zy
 Source Server Type    : Oracle
 Source Server Version : 190000
 Source Host           : 127.0.0.1:1521
 Source Schema         : C##ZY

 Target Server Type    : Oracle
 Target Server Version : 190000
 File Encoding         : 65001

 Date: 28/07/2022 17:16:19
*/


-- ----------------------------
-- Table structure for DICT
-- ----------------------------
DROP TABLE "C##ZY"."DICT";
CREATE TABLE "C##ZY"."DICT" (
  "DICT_ID" NUMBER(18,0) VISIBLE NOT NULL,
  "DICT_NAME" VARCHAR2(255 BYTE) VISIBLE NOT NULL,
  "DESCRIPTION" VARCHAR2(255 BYTE) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."DICT"."DICT_ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."DICT"."DICT_NAME" IS '字典名称';
COMMENT ON COLUMN "C##ZY"."DICT"."DESCRIPTION" IS '描述';

-- ----------------------------
-- Records of DICT
-- ----------------------------
INSERT INTO "C##ZY"."DICT" VALUES ('11', 'customer_type', '客户类型');
INSERT INTO "C##ZY"."DICT" VALUES ('1', 'product_class', '产品类别');
INSERT INTO "C##ZY"."DICT" VALUES ('2', 'poduct_stage', '开发阶段');
INSERT INTO "C##ZY"."DICT" VALUES ('3', 'product_sale', '项目所属销售条线');
INSERT INTO "C##ZY"."DICT" VALUES ('4', 'product_strategy', '产品投资策略');
INSERT INTO "C##ZY"."DICT" VALUES ('5', 'product_type', '产品投资类型');
INSERT INTO "C##ZY"."DICT" VALUES ('6', 'is_performance_compensation', '是否提取业绩报酬');
INSERT INTO "C##ZY"."DICT" VALUES ('7', 'product_valuation', '产品估值方法');
INSERT INTO "C##ZY"."DICT" VALUES ('8', 'is_corrections', '是否补正');
INSERT INTO "C##ZY"."DICT" VALUES ('9', 'product_maintain_stage', '产品维护阶段');
INSERT INTO "C##ZY"."DICT" VALUES ('10', 'product_state', '产品状态');

-- ----------------------------
-- Table structure for DICT_DETAIL
-- ----------------------------
DROP TABLE "C##ZY"."DICT_DETAIL";
CREATE TABLE "C##ZY"."DICT_DETAIL" (
  "DETAIL_ID" NUMBER(18,0) VISIBLE NOT NULL,
  "DICT_ID" NUMBER(18,0) VISIBLE,
  "VALUE" VARCHAR2(255 BYTE) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."DICT_DETAIL"."DETAIL_ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."DICT_DETAIL"."DICT_ID" IS '字典ID';
COMMENT ON COLUMN "C##ZY"."DICT_DETAIL"."VALUE" IS '字典值';

-- ----------------------------
-- Records of DICT_DETAIL
-- ----------------------------
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('35', '11', '银行-理财');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('36', '11', '银行-自营');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('37', '11', '保险');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('38', '11', '养老金');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('39', '11', '劵商期货');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('40', '11', '信托');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('41', '11', '非银-企业');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('42', '11', '财务公司');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('43', '11', '大学基金');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('44', '11', '高净值');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('45', '11', '其他');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('1', '1', '类别一');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('2', '1', '类别二');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('3', '2', '产品要素沟通');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('4', '2', '产品立项');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('5', '2', '产品推进中');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('6', '2', 'IPO及成立备案');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('7', '3', '机构');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('8', '3', '零售');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('9', '3', '电商');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('10', '4', '权益');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('11', '4', '固收');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('12', '4', '固收+');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('13', '4', '海外权益');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('14', '4', '量化对冲');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('15', '4', '指数增强');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('16', '4', 'FOF');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('17', '4', '其他');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('18', '5', '权益类');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('19', '5', '固定收益类');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('20', '5', '混合类');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('21', '5', '商品及金融衍生品类');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('22', '6', '是');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('23', '6', '否');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('24', '7', '市值法');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('25', '7', '摊余成本法');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('26', '8', '是');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('27', '8', '否');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('28', '9', '变更推进中');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('29', '9', '变更备案');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('30', '9', '展期备案(函件邮件)');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('31', '9', '终止及清盘备案');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('32', '10', '运作开放');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('33', '10', '运作封闭');
INSERT INTO "C##ZY"."DICT_DETAIL" VALUES ('34', '10', '其他');

-- ----------------------------
-- Table structure for PRODUCT
-- ----------------------------
DROP TABLE "C##ZY"."PRODUCT";
CREATE TABLE "C##ZY"."PRODUCT" (
  "PRODUCT_ID" NUMBER(18,0) VISIBLE NOT NULL,
  "PRODUCT_NAME" VARCHAR2(255 BYTE) VISIBLE,
  "PRODUCT_CLASS" VARCHAR2(255 BYTE) VISIBLE,
  "OPERATES" VARCHAR2(255 BYTE) VISIBLE,
  "CONTRACT_TIME" DATE VISIBLE,
  "ESTIMATED_SCALE" VARCHAR2(255 BYTE) VISIBLE,
  "ISSUANCE_SCALE" VARCHAR2(255 BYTE) VISIBLE,
  "IS_TEMPLATE" VARCHAR2(255 BYTE) VISIBLE,
  "STAGE" VARCHAR2(255 BYTE) VISIBLE,
  "PRODUCT_STATE" VARCHAR2(255 BYTE) VISIBLE,
  "PRODUCT_MAINTENANCE_ID" NUMBER(18,0) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."PRODUCT"."PRODUCT_ID" IS '产品id';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."PRODUCT_NAME" IS '产品名称';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."PRODUCT_CLASS" IS '产品类别';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."OPERATES" IS '运作方式';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."CONTRACT_TIME" IS '合同期限';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."ESTIMATED_SCALE" IS '预计规模';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."ISSUANCE_SCALE" IS '发行规模';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."IS_TEMPLATE" IS '是否使用管理人合同模板';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."STAGE" IS '开发阶段';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."PRODUCT_STATE" IS '产品状态';
COMMENT ON COLUMN "C##ZY"."PRODUCT"."PRODUCT_MAINTENANCE_ID" IS '产品维护事项ID';

-- ----------------------------
-- Records of PRODUCT
-- ----------------------------
INSERT INTO "C##ZY"."PRODUCT" VALUES ('1', 'test1', 'test1', 'test', TO_DATE('2022-07-22 16:23:55', 'SYYYY-MM-DD HH24:MI:SS'), 'test', 'test', 'test', 'test', '其他', '1');
INSERT INTO "C##ZY"."PRODUCT" VALUES ('2', 'test2', 'test2', 'test2', TO_DATE('2022-07-26 11:02:36', 'SYYYY-MM-DD HH24:MI:SS'), 'test2', 'test2', 'test2', 'test2', '其他', '1');
INSERT INTO "C##ZY"."PRODUCT" VALUES ('3', 'test3', 'test3', 'test3', TO_DATE('2022-07-26 11:03:26', 'SYYYY-MM-DD HH24:MI:SS'), 'test3', 'test3', 'test3', 'test3', '开放运作', '1');
INSERT INTO "C##ZY"."PRODUCT" VALUES ('4', 'test4', 'test4', 'test4', TO_DATE('2022-07-26 11:03:45', 'SYYYY-MM-DD HH24:MI:SS'), 'test4', 'test4', 'test4', 'test4', '开放封闭', '1');
INSERT INTO "C##ZY"."PRODUCT" VALUES ('5', 'test5', 'test5', 'test5', TO_DATE('2022-07-26 11:04:36', 'SYYYY-MM-DD HH24:MI:SS'), 'test5', 'test5', 'test5', 'test5', '其他', '1');
INSERT INTO "C##ZY"."PRODUCT" VALUES ('6', 'test6', 'test6', 'test6', TO_DATE('2022-07-26 11:03:45', 'SYYYY-MM-DD HH24:MI:SS'), 'test6', 'test6', 'test6', 'test6', '开放封闭', NULL);
INSERT INTO "C##ZY"."PRODUCT" VALUES ('7', 'test7', 'test7', 'test7', TO_DATE('2022-07-26 11:03:45', 'SYYYY-MM-DD HH24:MI:SS'), 'test7', 'test7', 'test7', 'test7', '开放封闭', NULL);

-- ----------------------------
-- Table structure for PRODUCT_COST
-- ----------------------------
DROP TABLE "C##ZY"."PRODUCT_COST";
CREATE TABLE "C##ZY"."PRODUCT_COST" (
  "ID" NUMBER(18,0) VISIBLE NOT NULL,
  "PRODUCT_ID" NUMBER(18,0) VISIBLE,
  "COST_CLASS" VARCHAR2(255 BYTE) VISIBLE,
  "MANAGEMENT_FEE" NUMBER(10,0) VISIBLE,
  "TRUST_RATE" NUMBER(10,0) VISIBLE,
  "IS_PERFORMANCE_COMPENSATION" VARCHAR2(10 BYTE) VISIBLE,
  "DATUM" VARCHAR2(10 BYTE) VISIBLE,
  "SCALE" NUMBER(10,0) VISIBLE,
  "REMARKS" VARCHAR2(255 BYTE) VISIBLE,
  "OTHER_COST" NUMBER(18,0) VISIBLE,
  "METHOD" VARCHAR2(255 BYTE) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."PRODUCT_ID" IS '产品ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."COST_CLASS" IS '管理费类型';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."MANAGEMENT_FEE" IS '管理费率';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."TRUST_RATE" IS '托管费率';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."IS_PERFORMANCE_COMPENSATION" IS '是否提取业绩报酬';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."DATUM" IS '业绩报酬计提基准';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."SCALE" IS '业绩报酬计提比例';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."REMARKS" IS '业绩报酬备注';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."OTHER_COST" IS '其他费用';
COMMENT ON COLUMN "C##ZY"."PRODUCT_COST"."METHOD" IS '产品估值方法';

-- ----------------------------
-- Records of PRODUCT_COST
-- ----------------------------
INSERT INTO "C##ZY"."PRODUCT_COST" VALUES ('1', '1', '其他', '10', '10', '是', '10', '10', '无', NULL, '市值法');

-- ----------------------------
-- Table structure for PRODUCT_DATE
-- ----------------------------
DROP TABLE "C##ZY"."PRODUCT_DATE";
CREATE TABLE "C##ZY"."PRODUCT_DATE" (
  "ID" NUMBER(18,0) VISIBLE NOT NULL,
  "PRODUCT_ID" NUMBER(18,0) VISIBLE,
  "RECEIVE_DATE" DATE VISIBLE,
  "START_ITEM_DATE" DATE VISIBLE,
  "FIRST_DEAL_DATE" DATE VISIBLE,
  "ONE_EXAMINE_DATE" DATE VISIBLE,
  "TWO_EXAMINE_DATE" DATE VISIBLE,
  "START_CONTRACT_DATE" DATE VISIBLE,
  "FINISH_CONTRACT_DATE" DATE VISIBLE,
  "STAR_DATE" DATE VISIBLE,
  "END_DATE" DATE VISIBLE,
  "ESTABLISH_DATE" DATE VISIBLE,
  "FIRST_RECORD_DATE" DATE VISIBLE,
  "PASS_RECORD_DATE" DATE VISIBLE,
  "CLEAR_DATE" DATE VISIBLE,
  "CLEAR_RECORD_DATE" DATE VISIBLE,
  "MAIL_DATE" DATE VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."PRODUCT_ID" IS '产品ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."RECEIVE_DATE" IS '收到需求日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."START_ITEM_DATE" IS '发起立项日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."FIRST_DEAL_DATE" IS '第一次处理立项日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."ONE_EXAMINE_DATE" IS '
发起第一轮审核日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."TWO_EXAMINE_DATE" IS '发起第二轮审核日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."START_CONTRACT_DATE" IS '发起产品合同日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."FINISH_CONTRACT_DATE" IS '完成合同签署日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."STAR_DATE" IS '发行起始日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."END_DATE" IS '发行结束日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."ESTABLISH_DATE" IS '计划成立日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."FIRST_RECORD_DATE" IS '首次备案日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."PASS_RECORD_DATE" IS '备案通过日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."CLEAR_DATE" IS '完成清算日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."CLEAR_RECORD_DATE" IS '清算备案日';
COMMENT ON COLUMN "C##ZY"."PRODUCT_DATE"."MAIL_DATE" IS '函件邮件日';

-- ----------------------------
-- Records of PRODUCT_DATE
-- ----------------------------
INSERT INTO "C##ZY"."PRODUCT_DATE" VALUES ('1', '1', TO_DATE('2022-07-22 13:57:20', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:23', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:26', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:29', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:32', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:34', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:37', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:42', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:44', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:46', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:48', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:50', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:52', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:54', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2022-07-22 13:57:57', 'SYYYY-MM-DD HH24:MI:SS'));

-- ----------------------------
-- Table structure for PRODUCT_INVESTMENT
-- ----------------------------
DROP TABLE "C##ZY"."PRODUCT_INVESTMENT";
CREATE TABLE "C##ZY"."PRODUCT_INVESTMENT" (
  "ID" NUMBER(18,0) VISIBLE NOT NULL,
  "PRODUCT_ID" NUMBER(18,0) VISIBLE NOT NULL,
  "INVESTMENT_MANAGER" VARCHAR2(255 BYTE) VISIBLE,
  "INVESTMENT_GROUP" VARCHAR2(255 BYTE) VISIBLE,
  "INVESTMENT_CLASS" VARCHAR2(255 BYTE) VISIBLE,
  "INVESTMENT_STRATEGY" VARCHAR2(255 BYTE) VISIBLE,
  "SUPERIOR_LIMIT" NUMBER(10,0) VISIBLE,
  "LOWER_LIMIT" NUMBER(10,0) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."PRODUCT_ID" IS '产品ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."INVESTMENT_MANAGER" IS '投资经理';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."INVESTMENT_GROUP" IS '投研团队';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."INVESTMENT_CLASS" IS '产品投资类型';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."INVESTMENT_STRATEGY" IS '主要投资策略';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."SUPERIOR_LIMIT" IS '权益比例上限';
COMMENT ON COLUMN "C##ZY"."PRODUCT_INVESTMENT"."LOWER_LIMIT" IS '权益比例下限';

-- ----------------------------
-- Records of PRODUCT_INVESTMENT
-- ----------------------------
INSERT INTO "C##ZY"."PRODUCT_INVESTMENT" VALUES ('1', '1', 'test', '固收', '固定收益类', '固收', '10', '5');

-- ----------------------------
-- Table structure for PRODUCT_MAINTENANCE
-- ----------------------------
DROP TABLE "C##ZY"."PRODUCT_MAINTENANCE";
CREATE TABLE "C##ZY"."PRODUCT_MAINTENANCE" (
  "ID" NUMBER(18,0) VISIBLE NOT NULL,
  "PRODUCT_ID" NUMBER(18,0) VISIBLE,
  "MAINTENANCE_MESSAGE" VARCHAR2(255 BYTE) VISIBLE,
  "STAGE" VARCHAR2(255 BYTE) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."PRODUCT_MAINTENANCE"."ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_MAINTENANCE"."PRODUCT_ID" IS '产品ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_MAINTENANCE"."MAINTENANCE_MESSAGE" IS '维护事项名称';
COMMENT ON COLUMN "C##ZY"."PRODUCT_MAINTENANCE"."STAGE" IS '产品维护阶段';

-- ----------------------------
-- Records of PRODUCT_MAINTENANCE
-- ----------------------------
INSERT INTO "C##ZY"."PRODUCT_MAINTENANCE" VALUES ('1', '1', 'test', '终止及清盘备案');

-- ----------------------------
-- Table structure for PRODUCT_SALE
-- ----------------------------
DROP TABLE "C##ZY"."PRODUCT_SALE";
CREATE TABLE "C##ZY"."PRODUCT_SALE" (
  "ID" NUMBER(18,0) VISIBLE NOT NULL,
  "CONSIGNOR" VARCHAR2(255 BYTE) VISIBLE NOT NULL,
  "CUSTODIAN" VARCHAR2(255 BYTE) VISIBLE NOT NULL,
  "CUSTOMER_TYPE" VARCHAR2(255 BYTE) VISIBLE,
  "SALESPERSON" VARCHAR2(255 BYTE) VISIBLE,
  "SALES_LINE" VARCHAR2(255 BYTE) VISIBLE,
  "PRODUCT_ID" NUMBER(18,0) VISIBLE NOT NULL
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."ID" IS 'ID';
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."CONSIGNOR" IS '委托人名称';
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."CUSTODIAN" IS '托管人';
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."CUSTOMER_TYPE" IS '客户类型';
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."SALESPERSON" IS '项目负责销售';
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."SALES_LINE" IS '项目所属销售条线';
COMMENT ON COLUMN "C##ZY"."PRODUCT_SALE"."PRODUCT_ID" IS '产品ID';

-- ----------------------------
-- Records of PRODUCT_SALE
-- ----------------------------
INSERT INTO "C##ZY"."PRODUCT_SALE" VALUES ('1', 'test', '境内托管机构', '财务公司', 'people1', '机构', '1');

-- ----------------------------
-- Sequence structure for product_seq
-- ----------------------------
DROP SEQUENCE "C##ZY"."product_seq";
CREATE SEQUENCE "C##ZY"."product_seq" MINVALUE 1 MAXVALUE 99999999999999999 INCREMENT BY 1 ORDER NOCACHE;

-- ----------------------------
-- Primary Key structure for table DICT
-- ----------------------------
ALTER TABLE "C##ZY"."DICT" ADD CONSTRAINT "SYS_C007440" PRIMARY KEY ("DICT_ID");

-- ----------------------------
-- Checks structure for table DICT
-- ----------------------------
ALTER TABLE "C##ZY"."DICT" ADD CONSTRAINT "SYS_C007438" CHECK ("DICT_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "C##ZY"."DICT" ADD CONSTRAINT "SYS_C007439" CHECK ("DICT_NAME" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table DICT_DETAIL
-- ----------------------------
ALTER TABLE "C##ZY"."DICT_DETAIL" ADD CONSTRAINT "SYS_C007443" PRIMARY KEY ("DETAIL_ID");

-- ----------------------------
-- Checks structure for table DICT_DETAIL
-- ----------------------------
ALTER TABLE "C##ZY"."DICT_DETAIL" ADD CONSTRAINT "SYS_C007441" CHECK ("DETAIL_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table PRODUCT
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT" ADD CONSTRAINT "SYS_C007420" PRIMARY KEY ("PRODUCT_ID");

-- ----------------------------
-- Checks structure for table PRODUCT
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT" ADD CONSTRAINT "SYS_C007419" CHECK ("PRODUCT_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Triggers structure for table PRODUCT
-- ----------------------------
CREATE TRIGGER "C##ZY"."product_trigger" BEFORE INSERT ON "C##ZY"."PRODUCT" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW 
BEGIN
select "product_seq".nextval into:new."PRODUCT_ID" from dual;
END;
/

-- ----------------------------
-- Primary Key structure for table PRODUCT_COST
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_COST" ADD CONSTRAINT "SYS_C007453" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table PRODUCT_COST
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_COST" ADD CONSTRAINT "SYS_C007452" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table PRODUCT_DATE
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_DATE" ADD CONSTRAINT "SYS_C007455" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table PRODUCT_DATE
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_DATE" ADD CONSTRAINT "SYS_C007454" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table PRODUCT_INVESTMENT
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_INVESTMENT" ADD CONSTRAINT "SYS_C007451" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table PRODUCT_INVESTMENT
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_INVESTMENT" ADD CONSTRAINT "SYS_C007449" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "C##ZY"."PRODUCT_INVESTMENT" ADD CONSTRAINT "SYS_C007450" CHECK ("PRODUCT_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table PRODUCT_MAINTENANCE
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_MAINTENANCE" ADD CONSTRAINT "SYS_C007457" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table PRODUCT_MAINTENANCE
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_MAINTENANCE" ADD CONSTRAINT "SYS_C007456" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table PRODUCT_SALE
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_SALE" ADD CONSTRAINT "SYS_C007447" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table PRODUCT_SALE
-- ----------------------------
ALTER TABLE "C##ZY"."PRODUCT_SALE" ADD CONSTRAINT "SYS_C007444" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "C##ZY"."PRODUCT_SALE" ADD CONSTRAINT "SYS_C007445" CHECK ("CONSIGNOR" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "C##ZY"."PRODUCT_SALE" ADD CONSTRAINT "SYS_C007446" CHECK ("CUSTODIAN" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "C##ZY"."PRODUCT_SALE" ADD CONSTRAINT "SYS_C007448" CHECK ("PRODUCT_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
