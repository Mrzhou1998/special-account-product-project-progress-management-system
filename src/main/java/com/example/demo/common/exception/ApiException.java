package com.example.demo.common.exception;

import com.example.demo.common.enums.ExceptionEnum;
import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/28
 */
@Data
public class ApiException extends RuntimeException {

    private ExceptionEnum exceptionEnum;
    private String code;
    private String errorMessage;

    public ApiException() {
        super();
    }

    public ApiException(ExceptionEnum exceptionEnum) {
        super("{code:" + exceptionEnum.getCode() + ",errorMessage:" + exceptionEnum.getMessage() + "}");
        this.exceptionEnum = exceptionEnum;
        this.code = exceptionEnum.getCode();
        this.errorMessage = exceptionEnum.getMessage();
    }

    public ApiException(String code, String errorMessage, Object... args) {
        super("{code:" + code + ",errorMessage:" + String.format(errorMessage, args) + "}");
        this.code = code;
        this.errorMessage = String.format(errorMessage, args);
    }

}
