package com.example.demo.common.api;

import com.example.demo.common.enums.ExceptionEnum;
import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/28
 */
@Data
public class ApiResult<T> {
    private String code;
    private String message;
    private T data;

    public ApiResult() {
    }

    public ApiResult(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 成功
     */
    public static <T> ApiResult<T> success(T data) {
        ApiResult<T> result = new ApiResult<T>();
        result.setCode(ExceptionEnum.SUCCESS.getCode());
        result.setMessage(ExceptionEnum.SUCCESS.getMessage());
        result.setData(data);
        return result;
    }

    /**
     * 失败
     */
    public static <T> ApiResult<T> error(String code, String message) {
        return new ApiResult(code, message);
    }
}
