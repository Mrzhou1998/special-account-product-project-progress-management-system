package com.example.demo.common.enums;

/**
 * @Author Violet
 * @Data 2022/7/28
 */
public enum ExceptionEnum {

    /**
     * 状态
     */
    SUCCESS("0", "成功"),
    FAIL("-1", "失败"),
    /**
     * 400
     */
    BAD_REQUEST("400", "请求数据格式不正确!"),
    UNAUTHORIZED("401", "登录凭证过期!"),
    FORBIDDEN("403", "没有访问权限!"),
    NOT_FOUND("404", "请求的资源找不到!"),
    /**
     * 500
     */
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
    SERVICE_UNAVAILABLE("503", "服务器正忙，请稍后再试!"),
    /**
     * 未知
     */
    UNKNOWN("10000", "未知异常!");

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误描述
     */
    private String message;

    ExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
