package com.example.demo.dict.service;

import com.example.demo.dict.dto.DictRequest;
import com.example.demo.dict.repo.entity.Dict;

import java.util.List;


/**
 * @Author Violet
 * @Data 2022/7/21
 */
public interface DictService {
    /**
     * 新增字典
     *
     * @param dictVO
     * @return boolean
     */
    Boolean insertOneDict(DictRequest dictVO);

    /**
     * 根据ID查询字典
     *
     * @param id
     * @return Dict
     */
    Dict getDictById(Long id);

    /**
     * 根据描述获取字典信息
     *
     * @param description
     * @return
     */
    Dict getDictByDescription(String description);

    /**
     * 获取字典列表
     *
     * @return List
     */
    List<Dict> getListDict();
}
