package com.example.demo.dict.service.impl;

import com.example.demo.dict.dto.DictDetailRequest;
import com.example.demo.dict.repo.entity.DictDetail;
import com.example.demo.dict.repo.mapper.DictDetailMapper;
import com.example.demo.dict.service.DictDetailService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class DictDetailServiceImpl implements DictDetailService {

    @Autowired
    DictDetailMapper dictDetailMapper;

    @Override
    public List<DictDetail> getByDictId(Long id) {
        return dictDetailMapper.selectByDictId(id);
    }

    @Override
    public Boolean addDictDetail(DictDetailRequest request) {
        DictDetail dictDetail = new DictDetail();
        BeanUtils.copyProperties(request, dictDetail);
        return dictDetailMapper.insert(dictDetail);
    }
}
