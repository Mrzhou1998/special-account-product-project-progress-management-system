package com.example.demo.dict.service;

import com.example.demo.dict.dto.DictDetailRequest;
import com.example.demo.dict.repo.entity.DictDetail;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface DictDetailService {

    /**
     * 通过字典ID获取字典详情列表
     * @param id
     * @return
     */
    List<DictDetail> getByDictId(Long id);

    Boolean addDictDetail(DictDetailRequest request);
}
