package com.example.demo.dict.service.impl;

import com.example.demo.dict.dto.DictRequest;
import com.example.demo.dict.repo.entity.Dict;
import com.example.demo.dict.repo.mapper.DictMapper;
import com.example.demo.dict.service.DictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/21
 */
@Service
public class DictServiceImpl implements DictService {

    @Autowired
    DictMapper dictMapper;

    @Override
    public Boolean insertOneDict(DictRequest dictBO) {
        Dict dict = new Dict();
        dict.setDictName(dictBO.getName());
        dict.setDescription(dictBO.getDescription());
        return dictMapper.insert(dict);
    }

    @Override
    public Dict getDictById(Long id) {
        return dictMapper.selectByPrimaryKey(id);
    }

    @Override
    public Dict getDictByDescription(String description) {
        return dictMapper.selectByDescription(description);
    }

    @Override
    public List<Dict> getListDict() {
        return dictMapper.selectAllDict();
    }
}
