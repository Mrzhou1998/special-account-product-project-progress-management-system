package com.example.demo.dict.controller;

import com.example.demo.dict.dto.DictDetailRequest;
import com.example.demo.dict.repo.entity.DictDetail;
import com.example.demo.dict.service.DictDetailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/23
 */

@RestController
@RequestMapping("/api/dictDetail")
public class DictDetailController {

    @Autowired
    DictDetailService dictDetailService;

    @ApiOperation("通过字典ID获得字典详情")
    @RequestMapping(value = "/getForDict", method = RequestMethod.POST)
    @ResponseBody
    public List<DictDetail> getForDict(@RequestBody @Validated Long id) {

        return dictDetailService.getByDictId(id);
    }

    @ApiOperation("新增字典详情")
    @RequestMapping(value = "/addDictDetail", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addDictDetail(@RequestBody @Validated DictDetailRequest request) {
        return dictDetailService.addDictDetail(request);
    }
}
