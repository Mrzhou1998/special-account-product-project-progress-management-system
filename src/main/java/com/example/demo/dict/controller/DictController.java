package com.example.demo.dict.controller;

import com.example.demo.dict.dto.DictRequest;
import com.example.demo.dict.repo.entity.Dict;
import com.example.demo.dict.service.DictService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/21
 */
@RestController
@RequestMapping("/api/dict")
public class DictController {

    @Autowired
    private DictService dictService;

    @ApiOperation("添加字典")
    @RequestMapping(value = "/addOneDict", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addOneDict(@RequestBody @Validated DictRequest dictBO) {
        return dictService.insertOneDict(dictBO);
    }

    @ApiOperation("通过字典id获得字典信息")
    @RequestMapping(value = "/getOneDict", method = RequestMethod.POST)
    @ResponseBody
    public Dict getOneDict(@RequestBody @Validated Long id) {
        return dictService.getDictById(id);
    }

    @ApiOperation("通过字典名称获得字典信息")
    @RequestMapping(value = "/getOneDictByDescription", method = RequestMethod.POST)
    @ResponseBody
    public Dict getOneDict(@RequestBody @Validated String description) {
        return dictService.getDictByDescription(description);
    }


    @ApiOperation("获取字典列表")
    @RequestMapping(value = "/getAllDict", method = RequestMethod.POST)
    @ResponseBody
    public List<Dict> getAllDict() {
        return dictService.getListDict();
    }

}
