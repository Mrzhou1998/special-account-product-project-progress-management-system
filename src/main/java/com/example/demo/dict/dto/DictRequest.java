package com.example.demo.dict.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/21
 */
@Data
@Builder
public class DictRequest {

    /**
     * ID
     */
    private Long dictId;

    /**
     * 字典名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

}
