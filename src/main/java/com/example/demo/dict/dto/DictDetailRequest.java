package com.example.demo.dict.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Data
@Builder
public class DictDetailRequest {
    /**
     * ID
     */
    private Long detailId;

    /**
     * 字典ID
     */
    private Long dictId;

    /**
     * 字典值
     */
    private String value;
}
