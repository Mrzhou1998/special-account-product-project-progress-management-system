package com.example.demo.dict.repo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * DICT
 *
 * @author Violet
 */
@Data
public class Dict implements Serializable {
    /**
     * ID
     */
    private Long dictId;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 描述
     */
    private String description;

    private static final long serialVersionUID = 1L;
}