package com.example.demo.dict.repo.mapper;

import com.example.demo.dict.repo.entity.Dict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DictMapper {
    int deleteByPrimaryKey(Long dictId);

    Boolean insert(Dict record);

    int insertSelective(Dict record);

    Dict selectByPrimaryKey(Long dictId);

    int updateByPrimaryKeySelective(Dict record);

    int updateByPrimaryKey(Dict record);

    /**
     * 查询所有字典信息，ID升序输出
     *
     * @return List
     */
    @Select("select * from DICT order by DICT_ID asc ")
    List<Dict> selectAllDict();

    /**
     * 根据描述查询字典信息
     *
     * @param description
     * @return Dict
     */
    @Select("select * from DICT where description = #{description}")
    Dict selectByDescription(String description);

}