package com.example.demo.dict.repo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * DICT_DETAIL
 *
 * @author Violet
 */
@Data
public class DictDetail implements Serializable {
    /**
     * ID
     */
    private Long detailId;

    /**
     * 字典ID
     */
    private Long dictId;

    /**
     * 字典值
     */
    private String value;

    private static final long serialVersionUID = 1L;
}