package com.example.demo.dict.repo.mapper;

import com.example.demo.dict.repo.entity.DictDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Mapper
public interface DictDetailMapper {
    int deleteByPrimaryKey(Long detailId);

    Boolean insert(DictDetail record);

    int insertSelective(DictDetail record);

    DictDetail selectByPrimaryKey(Long detailId);

    int updateByPrimaryKeySelective(DictDetail record);

    int updateByPrimaryKey(DictDetail record);

    /**
     * 根据字典id查询字典详情，detailId升序
     *
     * @param dictId
     * @return List
     */
    @Select("select * from DICT_DETAIL where DICT_ID = to_number(#{dictId}) order by DETAIL_ID asc ")
    List<DictDetail> selectByDictId(Long dictId);

}