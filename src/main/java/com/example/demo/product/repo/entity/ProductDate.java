package com.example.demo.product.repo.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * PRODUCT_DATE
 *
 * @author Violet
 */
@Data
public class ProductDate implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 收到需求日
     */
    private Date receiveDate;

    /**
     * 发起立项日
     */
    private Date startItemDate;

    /**
     * 第一次处理立项日
     */
    private Date firstDealDate;

    /**
     * 发起第一轮审核日
     */
    private Date oneExamineDate;

    /**
     * 发起第二轮审核日
     */
    private Date twoExamineDate;

    /**
     * 发起产品合同日
     */
    private Date startContractDate;

    /**
     * 完成合同签署日
     */
    private Date finishContractDate;

    /**
     * 发行起始日
     */
    private Date starDate;

    /**
     * 发行结束日
     */
    private Date endDate;

    /**
     * 计划成立日
     */
    private Date establishDate;

    /**
     * 首次备案日
     */
    private Date firstRecordDate;

    /**
     * 备案通过日
     */
    private Date passRecordDate;

    /**
     * 完成清算日
     */
    private Date clearDate;

    /**
     * 清算备案日
     */
    private Date clearRecordDate;

    /**
     * 函件邮件日
     */
    private Date mailDate;

    private static final long serialVersionUID = 1L;
}