package com.example.demo.product.repo.mapper;

import com.example.demo.product.repo.entity.ProductCost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductCostMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProductCost record);

    int insertSelective(ProductCost record);

    ProductCost selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProductCost record);

    Boolean updateByPrimaryKey(ProductCost record);

    /**
     * 获取列表，产品id升序
     * @return
     */
    @Select("select * from PRODUCT_COST order by product_id asc ")
    List<ProductCost> selectAll();
}