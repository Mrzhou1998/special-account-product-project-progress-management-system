package com.example.demo.product.repo.mapper;

import com.example.demo.product.repo.entity.ProductInvestment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductInvestmentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProductInvestment record);

    int insertSelective(ProductInvestment record);

    ProductInvestment selectByPrimaryKey(Long id);

    Boolean updateByPrimaryKeySelective(ProductInvestment record);

    Boolean updateByPrimaryKey(ProductInvestment record);

    /**
     * 获取列表，产品id升序
     * @return
     */
    @Select("select * from PRODUCT_INVESTMENT order by product_id asc ")
    List<ProductInvestment> selectAll();
}