package com.example.demo.product.repo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * PRODUCT_MAINTENANCE
 *
 * @author Violet
 */
@Data
public class ProductMaintenance implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 维护事项名称
     */
    private String maintenanceMessage;

    /**
     * 产品维护阶段
     */
    private String stage;

    private static final long serialVersionUID = 1L;
}