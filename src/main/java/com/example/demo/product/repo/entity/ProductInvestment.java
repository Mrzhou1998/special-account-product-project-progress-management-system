package com.example.demo.product.repo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * PRODUCT_INVESTMENT
 *
 * @author Violet
 */
@Data
public class ProductInvestment implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 投资经理
     */
    private String investmentManager;

    /**
     * 投研团队
     */
    private String investmentGroup;

    /**
     * 产品投资类型
     */
    private String investmentClass;

    /**
     * 主要投资策略
     */
    private String investmentStrategy;

    /**
     * 权益比例上限
     */
    private Long superiorLimit;

    /**
     * 权益比例下限
     */
    private Long lowerLimit;

    private static final long serialVersionUID = 1L;
}