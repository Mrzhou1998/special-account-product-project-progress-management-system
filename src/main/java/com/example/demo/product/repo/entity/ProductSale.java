package com.example.demo.product.repo.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * PRODUCT_SALE
 *
 * @author Violet
 */
@Data
public class ProductSale implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 委托人名称
     */
    private String consignor;

    /**
     * 托管人
     */
    private String custodian;

    /**
     * 客户类型
     */
    private String customerType;

    /**
     * 项目负责销售
     */
    private String salesperson;

    /**
     * 项目所属销售条线
     */
    private String salesLine;

    /**
     * 产品ID
     */
    private Long productId;

    private static final long serialVersionUID = 1L;
}