package com.example.demo.product.repo.mapper;

import com.example.demo.product.repo.entity.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Violet
 */
@Mapper
public interface ProductMapper {
    int deleteByPrimaryKey(Long productId);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Long productId);

    Boolean updateByPrimaryKeySelective(Product record);

    Boolean updateByPrimaryKey(Product record);

    /**
     * 获取产品列表，id升序
     * @return
     */
    @Select("select * from PRODUCT order by product_id asc ")
    List<Product> selectAll();

    /**
     * 获取开发中的产品列表，id升序
     * @return
     */
    @Select("select * from PRODUCT where PRODUCT_STATE = '其他'order by product_id asc")
    List<Product> selectAllDoProduct();

    /**
     * 获取运作 开放/封闭 状态产品列表 id升序
     * @return
     */
    @Select("select * from PRODUCT where PRODUCT_STATE = '运作开放' OR PRODUCT_STATE = '运作封闭'order by product_id asc")
    List<Product> selectAllFinishProduct();
}