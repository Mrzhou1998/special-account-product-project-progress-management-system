package com.example.demo.product.repo.mapper;

import com.example.demo.product.repo.entity.ProductDate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductDateMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProductDate record);

    int insertSelective(ProductDate record);

    ProductDate selectByPrimaryKey(Long id);

    Boolean updateByPrimaryKeySelective(ProductDate record);

    Boolean updateByPrimaryKey(ProductDate record);

    /**
     * 获取列表，产品id升序
     * @return
     */
    @Select("select * from PRODUCT_DATE order by product_id asc ")
    List<ProductDate> selectAll();
}