package com.example.demo.product.repo.mapper;

import com.example.demo.product.repo.entity.ProductMaintenance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductMaintenanceMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProductMaintenance record);

    int insertSelective(ProductMaintenance record);

    ProductMaintenance selectByPrimaryKey(Long id);

    Boolean updateByPrimaryKeySelective(ProductMaintenance record);

    Boolean updateByPrimaryKey(ProductMaintenance record);

    /**
     * 获取列表，产品id升序
     * @return
     */
    @Select("select * from PRODUCT_MAINTENANCE order by product_id asc ")
    List<ProductMaintenance> selectAll();
}