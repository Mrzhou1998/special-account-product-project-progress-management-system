package com.example.demo.product.repo.mapper;

import com.example.demo.product.repo.entity.ProductSale;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;

@Mapper
public interface ProductSaleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProductSale record);

    int insertSelective(ProductSale record);

    ProductSale selectByPrimaryKey(Long id);

    Boolean updateByPrimaryKeySelective(ProductSale record);

    Boolean updateByPrimaryKey(ProductSale record);

    /**
     * 获取列表，id升序
     * @return
     */
    @Select("select * from PRODUCT_SALE order by ID asc ")
    List<ProductSale> selectAll();
}