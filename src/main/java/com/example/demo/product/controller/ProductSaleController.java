package com.example.demo.product.controller;

import com.example.demo.product.dto.ProductSaleRequest;
import com.example.demo.product.repo.entity.ProductSale;
import com.example.demo.product.service.ProductSaleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@RestController
@RequestMapping("/api/productSale")
public class ProductSaleController {
    @Autowired
    ProductSaleService productSaleService;

    @ApiOperation("获得项目销售信息")
    @RequestMapping(value = "/queryOneProductSale", method = RequestMethod.POST)
    @ResponseBody
    public ProductSale queryOneProductSale(@RequestBody @Validated Long id) {
        return productSaleService.getProductSaleById(id);
    }

    @ApiOperation("获取项目销售列表")
    @RequestMapping(value = "/queryAllProductSale", method = RequestMethod.POST)
    @ResponseBody
    public List<ProductSale> queryAllProductSale() {
        return productSaleService.getAllList();
    }

    @ApiOperation("新增项目销售")
    @RequestMapping(value = "/addProductSale", method = RequestMethod.POST)
    @ResponseBody
    public String addOneProductSale(@RequestBody @Validated ProductSaleRequest request) {
        return productSaleService.addOneProductSale(request);
    }

    @ApiOperation("更新项目销售信息")
    @RequestMapping(value = "/updateProductSale", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateProductSale(@RequestBody @Validated ProductSaleRequest request) {

        return productSaleService.updateProductSale(request);
    }
}
