package com.example.demo.product.controller;

import com.example.demo.product.dto.ProductMaintenanceRequest;
import com.example.demo.product.repo.entity.ProductMaintenance;
import com.example.demo.product.service.ProductMaintenanceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */

@RestController
@RequestMapping("/api/productMaintenance")
public class ProductMaintenanceController {

    @Autowired
    ProductMaintenanceService productMaintenanceService;

    @ApiOperation("获取产品维护信息")
    @RequestMapping(value = "/queryOneProductMaintenance", method = RequestMethod.POST)
    @ResponseBody
    public ProductMaintenance queryOneProductMaintenance(@RequestBody @Validated Long id) {
        return productMaintenanceService.getProductMaintenanceById(id);
    }

    @ApiOperation("获取产品维护信息列表")
    @RequestMapping(value = "/queryProductMaintenance", method = RequestMethod.POST)
    @ResponseBody
    public List<ProductMaintenance> queryProductMaintenance() {
        return productMaintenanceService.getAllProductMaintenance();
    }

    @ApiOperation("更新产品维护信息")
    @RequestMapping(value = "/updateProductMaintenance", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateProductMaintenance(@RequestBody @Validated ProductMaintenanceRequest request) {
        return productMaintenanceService.updateProductMaintenance(request);
    }

    @ApiOperation("新增产品维护信息")
    @RequestMapping(value = "/addProductMaintenance", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addProductMaintenance(@RequestBody @Validated ProductMaintenanceRequest request) {
        return productMaintenanceService.addOneProductMaintenance(request);
    }
}
