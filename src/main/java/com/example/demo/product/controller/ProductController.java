package com.example.demo.product.controller;

import com.example.demo.product.dto.ProductPageRequest;
import com.example.demo.product.dto.ProductRequest;
import com.example.demo.product.repo.entity.Product;
import com.example.demo.product.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @ApiOperation("获得一条产品信息")
    @RequestMapping(value = "/queryOneProduct", method = RequestMethod.POST)
    @ResponseBody
    public Product queryOneProduct(@RequestBody @Validated Long id) {
        return productService.getProductById(id);
    }

    @ApiOperation("获取产品列表")
    @RequestMapping(value = "/queryAllProduct", method = RequestMethod.POST)
    @ResponseBody
    public List<Product> queryAllProduct() {

        return productService.getAllList();
    }

    @ApiOperation("分页获取产品列表")
    @RequestMapping(value = "/queryPageProduct", method = RequestMethod.POST)
    @ResponseBody
    public List<Product> queryPageProduct(@RequestBody @Validated ProductPageRequest request) {

        return productService.pageProduct(request);
    }

    @ApiOperation("获取开发状态产品列表")
    @RequestMapping(value = "/queryAllDoProduct", method = RequestMethod.POST)
    @ResponseBody
    public List<Product> queryAllDoProduct() {

        return productService.getDoProduct();
    }

    @ApiOperation("获取状态运作 开放/封闭 的产品列表")
    @RequestMapping(value = "/queryAllFinishProduct", method = RequestMethod.POST)
    @ResponseBody
    public List<Product> queryAllFinishProduct() {

        return productService.getFinishProduct();
    }

    @ApiOperation("新增产品")
    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    @ResponseBody
    public String addOneProduct(@RequestBody @Validated ProductRequest request) {

        return productService.addOneProduct(request);
    }

    @ApiOperation("更新产品信息")
    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateProduct(@RequestBody @Validated ProductRequest request) {

        return productService.updateProduct(request);
    }
}
