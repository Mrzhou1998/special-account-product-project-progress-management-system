package com.example.demo.product.controller;

import com.example.demo.product.dto.ProductCostRequest;
import com.example.demo.product.repo.entity.ProductCost;
import com.example.demo.product.service.ProductCostService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@RestController
@RequestMapping("/api/product")
public class ProductCostController {

    @Autowired
    ProductCostService productCostService;

    @ApiOperation("获取费率估值信息")
    @RequestMapping(value = "/queryOneProductCost", method = RequestMethod.POST)
    @ResponseBody
    public ProductCost queryOneProductCost(@RequestBody @Validated Long id) {
        return productCostService.getProductCostById(id);
    }

    @ApiOperation("获取费率估值信息列表")
    @RequestMapping(value = "/queryProductCost", method = RequestMethod.POST)
    @ResponseBody
    public List<ProductCost> queryProductCost() {
        return productCostService.getAllProductCost();
    }

    @ApiOperation("更新费率估值信息")
    @RequestMapping(value = "/updateProductCost", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateProductCost(@RequestBody @Validated ProductCostRequest request) {
        return productCostService.updateProductCost(request);
    }

    @ApiOperation("新增费率估值信息")
    @RequestMapping(value = "/addProductCost", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addProductCost(@RequestBody @Validated ProductCostRequest request) {
        return productCostService.addOneProductCost(request);
    }


}
