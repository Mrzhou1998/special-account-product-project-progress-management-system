package com.example.demo.product.controller;

import com.example.demo.product.dto.ProductDateRequest;
import com.example.demo.product.repo.entity.ProductDate;
import com.example.demo.product.service.ProductDateService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@RestController
@RequestMapping("/api/productDate")
public class ProductDateController {

    @Autowired
    ProductDateService productDateService;

    @ApiOperation("获取产品日期相关信息")
    @RequestMapping(value = "/queryOneProductCost", method = RequestMethod.POST)
    @ResponseBody
    public ProductDate queryProductCost(@RequestBody @Validated Long id) {
        return productDateService.getProductDateById(id);
    }

    @ApiOperation("获取产品日期相关信息列表")
    @RequestMapping(value = "/queryProductCost", method = RequestMethod.POST)
    @ResponseBody
    public List<ProductDate> queryProductCost() {
        return productDateService.getAllProductDate();
    }

    @ApiOperation("新增产品日期相关信息")
    @RequestMapping(value = "/addProductCost", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addProductDate(@RequestBody @Validated ProductDateRequest request) {
        return productDateService.addOneProductDate(request);
    }


    @ApiOperation("更新产品日期相关信息")
    @RequestMapping(value = "/updateProductCost", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateProductCost(@RequestBody @Validated ProductDateRequest request) {
        return productDateService.updateProductDate(request);
    }

}
