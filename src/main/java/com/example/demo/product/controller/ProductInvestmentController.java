package com.example.demo.product.controller;


import com.example.demo.product.dto.ProductInvestmentRequest;
import com.example.demo.product.repo.entity.ProductInvestment;
import com.example.demo.product.service.ProductInvestmentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@RestController
@RequestMapping("/api/productInvestment")
public class ProductInvestmentController {

    @Autowired
    ProductInvestmentService productInvestmentService;

    @ApiOperation("获取投资以及投研团队信息")
    @RequestMapping(value = "/queryOneProductInvestment", method = RequestMethod.POST)
    @ResponseBody
    public ProductInvestment queryOneProductInvestment(@RequestBody @Validated Long id) {
        return productInvestmentService.getProductInvestmentById(id);
    }

    @ApiOperation("获取投资以及投研团队信息列表")
    @RequestMapping(value = "/queryProductInvestment", method = RequestMethod.POST)
    @ResponseBody
    public List<ProductInvestment> queryProductInvestment() {
        return productInvestmentService.getAllProductInvestment();
    }

    @ApiOperation("更新投资以及投研团队信息")
    @RequestMapping(value = "/updateProductInvestment", method = RequestMethod.POST)
    @ResponseBody
    public Boolean updateProductInvestment(@RequestBody @Validated ProductInvestmentRequest request) {
        return productInvestmentService.updateProductInvestment(request);
    }

    @ApiOperation("新增投资以及投研团队信息")
    @RequestMapping(value = "/addProductInvestment", method = RequestMethod.POST)
    @ResponseBody
    public Boolean addProductInvestment(@RequestBody @Validated ProductInvestmentRequest request) {
        return productInvestmentService.addOneProductInvestment(request);
    }
}
