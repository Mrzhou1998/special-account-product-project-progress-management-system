package com.example.demo.product.service.impl;

import com.example.demo.product.dto.ProductInvestmentRequest;
import com.example.demo.product.repo.entity.ProductInvestment;
import com.example.demo.product.repo.mapper.ProductInvestmentMapper;
import com.example.demo.product.service.ProductInvestmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class ProductInvestmentServiceImpl implements ProductInvestmentService {

    @Autowired
    ProductInvestmentMapper mapper;

    @Override
    public ProductInvestment getProductInvestmentById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ProductInvestment> getAllProductInvestment() {
        return mapper.selectAll();
    }

    @Override
    public Boolean addOneProductInvestment(ProductInvestmentRequest request) {
        ProductInvestment productInvestment = new ProductInvestment();
        BeanUtils.copyProperties(request, productInvestment);
        return mapper.insert(productInvestment) != 0;
    }

    @Override
    public Boolean updateProductInvestment(ProductInvestmentRequest request) {
        ProductInvestment productInvestment = new ProductInvestment();
        BeanUtils.copyProperties(request, productInvestment);
        return mapper.updateByPrimaryKey(productInvestment);
    }
}
