package com.example.demo.product.service.impl;

import com.example.demo.product.dto.ProductMaintenanceRequest;
import com.example.demo.product.repo.entity.ProductMaintenance;
import com.example.demo.product.repo.mapper.ProductMaintenanceMapper;
import com.example.demo.product.service.ProductMaintenanceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class ProductMaintenanceServiceImpl implements ProductMaintenanceService {

    @Autowired
    ProductMaintenanceMapper mapper;

    @Override
    public ProductMaintenance getProductMaintenanceById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ProductMaintenance> getAllProductMaintenance() {
        return mapper.selectAll();
    }

    @Override
    public Boolean addOneProductMaintenance(ProductMaintenanceRequest request) {
        ProductMaintenance productMaintenance = new ProductMaintenance();
        BeanUtils.copyProperties(request, productMaintenance);
        return mapper.insert(productMaintenance) != 0;
    }

    @Override
    public Boolean updateProductMaintenance(ProductMaintenanceRequest request) {
        ProductMaintenance productMaintenance = new ProductMaintenance();
        BeanUtils.copyProperties(request, productMaintenance);
        return mapper.updateByPrimaryKey(productMaintenance);
    }
}
