package com.example.demo.product.service;

import com.example.demo.product.dto.ProductPageRequest;
import com.example.demo.product.dto.ProductRequest;
import com.example.demo.product.repo.entity.Product;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface ProductService {
    /**
     * 根据ID获取产品
     *
     * @param id
     * @return Product
     */
    Product getProductById(Long id);

    /**
     * 查询所有产品信息 ID升序
     *
     * @return List<Product>
     */
    List<Product> getAllList();

    /**
     * 添加产品
     *
     * @param request
     * @return Sting
     */
    String addOneProduct(ProductRequest request);

    /**
     * 更新产品信息
     *
     * @param request
     * @return true/false
     */
    Boolean updateProduct(ProductRequest request);

    /**
     * 获取开发中的产品列表
     *
     * @return
     */
    List<Product> getDoProduct();

    /**
     * 获取运作 开放/封闭 状态产品列表
     *
     * @return
     */
    List<Product> getFinishProduct();

    /**
     * 分页查询产品信息
     *
     * @param request
     * @return
     */
    List<Product> pageProduct(ProductPageRequest request);
}
