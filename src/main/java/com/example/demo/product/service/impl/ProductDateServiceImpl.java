package com.example.demo.product.service.impl;

import com.example.demo.product.dto.ProductDateRequest;
import com.example.demo.product.repo.entity.Product;
import com.example.demo.product.repo.entity.ProductDate;
import com.example.demo.product.repo.mapper.ProductDateMapper;
import com.example.demo.product.service.ProductDateService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class ProductDateServiceImpl implements ProductDateService {

    @Autowired
    ProductDateMapper mapper;

    @Override
    public ProductDate getProductDateById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ProductDate> getAllProductDate() {
        return mapper.selectAll();
    }

    @Override
    public Boolean addOneProductDate(ProductDateRequest request) {
        ProductDate productDate = new ProductDate();
        BeanUtils.copyProperties(request, productDate);
        return mapper.insert(productDate) != 0;
    }

    @Override
    public Boolean updateProductDate(ProductDateRequest request) {
        ProductDate productDate = new ProductDate();
        BeanUtils.copyProperties(request, productDate);
        return mapper.updateByPrimaryKey(productDate);
    }
}
