package com.example.demo.product.service;

import com.example.demo.product.dto.ProductCostRequest;
import com.example.demo.product.repo.entity.ProductCost;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface ProductCostService {

    ProductCost getProductCostById(Long id);

    List<ProductCost> getAllProductCost();

    Boolean addOneProductCost(ProductCostRequest request);

    Boolean updateProductCost(ProductCostRequest request);
}
