package com.example.demo.product.service;

import com.example.demo.product.dto.ProductRequest;
import com.example.demo.product.dto.ProductSaleRequest;
import com.example.demo.product.repo.entity.ProductSale;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface ProductSaleService {
    /**
     * 根据ID获取项目销售
     *
     * @param id
     * @return Product
     */
    ProductSale getProductSaleById(Long id);

    /**
     * 查询所有项目销售信息 ID升序
     *
     * @return List<Product>
     */
    List<ProductSale> getAllList();

    /**
     * 添加项目销售
     *
     * @param request
     * @return Sting
     */
    String addOneProductSale(ProductSaleRequest request);

    /**
     * 更新项目销售信息
     *
     * @param request
     * @return true/false
     */
    Boolean updateProductSale(ProductSaleRequest request);
}
