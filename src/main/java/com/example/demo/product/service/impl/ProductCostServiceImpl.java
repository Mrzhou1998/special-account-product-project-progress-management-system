package com.example.demo.product.service.impl;

import com.example.demo.product.dto.ProductCostRequest;
import com.example.demo.product.repo.entity.Product;
import com.example.demo.product.repo.entity.ProductCost;
import com.example.demo.product.repo.mapper.ProductCostMapper;
import com.example.demo.product.service.ProductCostService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class ProductCostServiceImpl implements ProductCostService {

    @Autowired
    ProductCostMapper mapper;

    @Override
    public ProductCost getProductCostById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ProductCost> getAllProductCost() {
        return mapper.selectAll();
    }

    @Override
    public Boolean addOneProductCost(ProductCostRequest request) {
        ProductCost productCost = new ProductCost();
        BeanUtils.copyProperties(request, productCost);
        return mapper.insert(productCost) != 0;
    }

    @Override
    public Boolean updateProductCost(ProductCostRequest request) {
        ProductCost productCost = new ProductCost();
        BeanUtils.copyProperties(request, productCost);
        return mapper.updateByPrimaryKey(productCost);
    }
}
