package com.example.demo.product.service.impl;

import com.example.demo.product.dto.ProductSaleRequest;
import com.example.demo.product.repo.entity.ProductSale;
import com.example.demo.product.repo.mapper.ProductSaleMapper;
import com.example.demo.product.service.ProductSaleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class ProductSaleServiceImpl implements ProductSaleService {
    @Autowired
    ProductSaleMapper mapper;

    @Override
    public ProductSale getProductSaleById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ProductSale> getAllList() {
        return mapper.selectAll();
    }

    @Override
    public String addOneProductSale(ProductSaleRequest request) {
        ProductSale sale = new ProductSale();
        BeanUtils.copyProperties(request, sale);
        if (mapper.insert(sale) != 0) {
            return String.format("产品id：[%d] 项目销售信息添加成功", request.getProductId());
        } else {
            return String.format("新增失败，请检查销售信息id [%d] 参数", request.getId());
        }
    }

    @Override
    public Boolean updateProductSale(ProductSaleRequest request) {
        ProductSale sale = new ProductSale();
        BeanUtils.copyProperties(request, sale);
        return mapper.updateByPrimaryKey(sale);
    }
}
