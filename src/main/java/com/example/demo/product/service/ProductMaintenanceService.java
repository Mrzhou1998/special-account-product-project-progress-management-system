package com.example.demo.product.service;


import com.example.demo.product.dto.ProductMaintenanceRequest;
import com.example.demo.product.repo.entity.ProductMaintenance;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface ProductMaintenanceService {
    ProductMaintenance getProductMaintenanceById(Long id);

    List<ProductMaintenance> getAllProductMaintenance();

    Boolean addOneProductMaintenance(ProductMaintenanceRequest request);

    Boolean updateProductMaintenance(ProductMaintenanceRequest request);
}
