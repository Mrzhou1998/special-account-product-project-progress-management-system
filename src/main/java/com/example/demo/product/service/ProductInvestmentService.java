package com.example.demo.product.service;

import com.example.demo.product.dto.ProductInvestmentRequest;
import com.example.demo.product.repo.entity.ProductInvestment;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface ProductInvestmentService {
    ProductInvestment getProductInvestmentById(Long id);

    List<ProductInvestment> getAllProductInvestment();

    Boolean addOneProductInvestment(ProductInvestmentRequest request);

    Boolean updateProductInvestment(ProductInvestmentRequest request);
}
