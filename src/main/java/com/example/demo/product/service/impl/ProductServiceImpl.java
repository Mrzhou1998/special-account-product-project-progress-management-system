package com.example.demo.product.service.impl;

import com.example.demo.product.dto.ProductPageRequest;
import com.example.demo.product.dto.ProductRequest;
import com.example.demo.product.repo.entity.Product;
import com.example.demo.product.repo.mapper.ProductMapper;
import com.example.demo.product.service.ProductService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductMapper mapper;

    @Override
    public Product getProductById(Long id) {
        return mapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Product> getAllList() {
        return mapper.selectAll();
    }

    @Override
    public String addOneProduct(ProductRequest productBO) {
        Product product = new Product();
        product.setProductName(productBO.getProductName());
        product.setProductClass(productBO.getProductClass());
        product.setProductState(productBO.getProductState());
        product.setOperates(productBO.getOperates());
        product.setContractTime(productBO.getContractTime());
        product.setEstimatedScale(productBO.getEstimatedScale());
        product.setIssuanceScale(productBO.getIssuanceScale());
        product.setIsTemplate(productBO.getIsTemplate());
        product.setStage(productBO.getStage());
        if (mapper.insert(product) != 0) {
            return String.format("产品：[%S] 插入成功", productBO.getProductName());
        } else {
            return String.format("新增失败，请检查 [%s] 参数", productBO.getProductName());
        }
    }

    @Override
    public Boolean updateProduct(ProductRequest request) {
        Product product = new Product();
        BeanUtils.copyProperties(request, product);
        return mapper.updateByPrimaryKey(product);
    }

    @Override
    public List<Product> getDoProduct() {
        return mapper.selectAllDoProduct();
    }

    @Override
    public List<Product> getFinishProduct() {
        return mapper.selectAllFinishProduct();
    }

    @Override
    public List<Product> pageProduct(ProductPageRequest request) {
        PageHelper.startPage(request.getPage(), request.getSize());
        List<Product> products = mapper.selectAll();
        PageInfo<Product> info =new PageInfo<>(products);
        return info.getList();
    }
}
