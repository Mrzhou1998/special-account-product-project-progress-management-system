package com.example.demo.product.service;


import com.example.demo.product.dto.ProductDateRequest;
import com.example.demo.product.repo.entity.ProductDate;

import java.util.List;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
public interface ProductDateService {

    ProductDate getProductDateById(Long id);

    List<ProductDate> getAllProductDate();

    Boolean addOneProductDate(ProductDateRequest request);

    Boolean updateProductDate(ProductDateRequest request);
}
