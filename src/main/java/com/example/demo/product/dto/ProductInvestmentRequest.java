package com.example.demo.product.dto;

import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Data
public class ProductInvestmentRequest {
    /**
     * ID
     */
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 投资经理
     */
    private String investmentManager;

    /**
     * 投研团队
     */
    private String investmentGroup;

    /**
     * 产品投资类型
     */
    private String investmentClass;

    /**
     * 主要投资策略
     */
    private String investmentStrategy;

    /**
     * 权益比例上限
     */
    private Long superiorLimit;

    /**
     * 权益比例下限
     */
    private Long lowerLimit;
}
