package com.example.demo.product.dto;

import lombok.Data;
import java.util.Date;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Data
public class ProductRequest {
    /**
     * 产品id
     */
    private Long productId;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 产品类别
     */
    private String productClass;

    /**
     * 运作方式
     */
    private String operates;

    /**
     * 合同期限
     */
    private Date contractTime;

    /**
     * 预计规模
     */
    private String estimatedScale;

    /**
     * 发行规模
     */
    private String issuanceScale;

    /**
     * 是否使用管理人合同模板
     */
    private String isTemplate;

    /**
     * 开发阶段
     */
    private String stage;

    /**
     * 产品状态
     */
    private String productState;

    /**
     * 产品维护事项ID
     */
    private Long productMaintenanceId;
}
