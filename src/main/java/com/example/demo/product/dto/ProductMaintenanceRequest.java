package com.example.demo.product.dto;

import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Data
public class ProductMaintenanceRequest {
    /**
     * ID
     */
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 维护事项名称
     */
    private String maintenanceMessage;

    /**
     * 产品维护阶段
     */
    private String stage;
}
