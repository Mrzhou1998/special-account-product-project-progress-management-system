package com.example.demo.product.dto;

import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/22
 */
@Data
public class ProductCostRequest {
    /**
     * ID
     */
    private Long id;

    /**
     * 产品ID
     */
    private Long productId;

    /**
     * 管理费类型
     */
    private String costClass;

    /**
     * 管理费率
     */
    private Long managementFee;

    /**
     * 托管费率
     */
    private Long trustRate;

    /**
     * 是否提取业绩报酬
     */
    private String isPerformanceCompensation;

    /**
     * 业绩报酬计提基准
     */
    private String datum;

    /**
     * 业绩报酬计提比例
     */
    private Long scale;

    /**
     * 业绩报酬备注
     */
    private String remarks;

    /**
     * 其他费用
     */
    private Long otherCost;

    /**
     * 产品估值方法
     */
    private String method;
}
