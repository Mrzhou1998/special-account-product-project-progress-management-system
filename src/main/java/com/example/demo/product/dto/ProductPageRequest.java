package com.example.demo.product.dto;


import lombok.Data;

/**
 * @Author Violet
 * @Data 2022/7/26
 */
@Data
public class ProductPageRequest {

    private Integer page;

    private Integer size;
}
