package com.example.demo.product.service;

import com.example.demo.product.dto.ProductPageRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * @Author Violet
 * @Data 2022/7/24
 */
@SpringBootTest
@RunWith(SpringRunner.class)
class ProductServiceTest {

    @Autowired
    ProductService service;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllList() {
        System.out.println(service.getAllList());
    }


}